four areas to talk about

The baron - music guy #engagement. - look up on soundcloud.

Tongue in cheek - answering criticisms. What features did they take away this time.
putting to rest the criticisms and address them one by one.

file browser
shell
builder

icon theme changes
changes to menus

Theme - speed 50% performance boost.

theme changes

control center shrinking change
sound control panel

Gnome builder
Caroline telegram email

https://feaneron.com/home/blog/
17:29
Ryan Gorley
Hear me?
17:32
Britt
https://gitlab.gnome.org/Teams/Engagement/release-video
TheBaron on irc #engagement
18:03


https://gitlab.gnome.org/Teams/Engagement/release-video

https://gitlab.gnome.org/Teams/Engagement/engagement-assets

General Features

Application Features

Files
Settings
Terminal
Builder
Web

Pinned tabs (GNOME/epiphany!242 (merged))
Enable WebKit subprocess sandboxes (GNOME/epiphany!348 (merged))
WebKit content filters for adblocker (GNOME/epiphany!178 (merged))


Calendar
Music
Fractal
Boxes
Documents
Contacts
Videos
Todo
Polari
Rhythmbox
Tweaks
Evolution
Geary
Notes


Developer Features

Flatpak
GTK
Glib



The following modules have a new version:
 - at-spi2-core (2.33.2 => 2.33.90)
 - cheese (3.32.1 => 3.33.90.1)
 - eog (3.33.3 => 3.33.90)
 - epiphany (3.33.4 => 3.33.90)
 - evolution-data-server (3.33.4 => 3.33.90)
 - gcr (3.28.1 => 3.33.4)
 - gjs (1.57.4 => 1.57.90)
 - glib (2.61.1 => 2.61.2)
 - glib-networking (2.61.2 => 2.61.90)
 - gnome-bluetooth (3.32.1 => 3.33.90)
 - gnome-boxes (3.33.2 => 3.33.3)
 - gnome-desktop (3.33.4 => 3.33.90)
 - gnome-disk-utility (3.33.3 => 3.33.90)
 - gnome-getting-started-docs (3.32.2 => 3.33.90)
 - gnome-initial-setup (3.33.4 => 3.33.90)
 - gnome-maps (3.33.4 => 3.33.90)
 - gnome-music (3.33.4 => 3.33.90)
 - gnome-settings-daemon (3.33.0 => 3.33.90)
 - gnome-terminal (3.33.3 => 3.33.90) (*)
 - gnome-user-docs (3.32.3 => 3.33.90)
 - gnome-video-effects (0.4.3 => 0.5.0)
 - gnome-weather (3.32.2 => 3.33.90)
 - gsettings-desktop-schemas (3.33.1 => 3.33.90)
 - gtk-doc (1.30 => 1.31)
 - gvfs (1.41.4 => 1.41.90)
 - libdazzle (3.33.3 => 3.33.4)
 - libgee (0.20.1 => 0.20.2)
 - libgudev (232 => 233)
 - librsvg (2.45.8 => 2.45.90)
 - libsoup (2.67.3 => 2.67.91)
 - orca (3.33.3 => 3.33.90)
 - pango (1.43.0 => 1.44.3)
 - pyatspi (2.33.2 => 2.33.90)
 - simple-scan (3.33.4 => 3.33.90)
 - totem (3.33.0 => 3.33.90)
 - vala (0.45.3 => 0.45.90)
 - vte (0.57.3 => 0.57.90) (*)
(*) No summarized news available

The following modules weren't upgraded in this release:
   adwaita-icon-theme, at-spi2-atk, atk, atkmm, baobab, cantarell-fonts,
   clutter, clutter-gst, clutter-gtk, cogl, dconf, evince, file-roller, folks,
   gcab, gdk-pixbuf, gdm, gedit, geocode-glib, gfbgraph, glibmm, gmime,
   gnome-autoar, gnome-backgrounds, gnome-calculator, gnome-calendar,
   gnome-characters, gnome-clocks, gnome-color-manager, gnome-contacts,
   gnome-control-center, gnome-font-viewer, gnome-keyring, gnome-logs,
   gnome-menus, gnome-online-accounts, gnome-online-miners, gnome-photos,
   gnome-screenshot, gnome-session, gnome-shell, gnome-shell-extensions,
   gnome-software, gnome-system-monitor, gnome-themes-extra, gnome-user-share,
   gobject-introspection, gom, grilo, grilo-plugins, gsound, gspell, gssdp,
   gtk, gtk+, gtk-vnc, gtkmm, gtksourceview, gupnp, gupnp-av, gupnp-dlna,
   json-glib, libchamplain, libcroco, libgdata, libgepub, libgnomekbd,
   libgovirt, libgsf, libgtop, libgweather, libgxps, libmediaart, libnotify,
   libpeas, libsecret, libsigc++, libzapojit, mm-common, mutter, nautilus,
   network-manager-applet, pangomm, phodav, pygobject, rest, rygel, sushi,
   totem-pl-parser, tracker, tracker-miners, vino, yelp, yelp-tools, yelp-xsl,
   zenity

========================================
  at-spi2-core
========================================

What's new in at-spi2-core 2.33.90:

* Refactor the API for the screen reader to notify listeners of its status.

* Add a sender to the AtspiEvent struct.

* Add missing atspi_application_get_type prototype.

* Support mutter remote desktop interface for synthesizing keyboard/mouse
  events (likely still needs work).


========================================
  cheese
========================================

version 3.33.91
  - Drop libcanberra-gtk (GTK 2) dependency introduced in previous release

version 3.33.90
  - Switch to meson build system (thanks Iñigo!)
  - New app icon
  - New keyboard shortcuts window
  - Many updated translations

========================================
  eog
========================================

Version 3.33.90
---------------

New and updated translations:

- Asier Sarasua Garmendia [eu]
- Jor Teron [mjw]
- Anders Jonsson [sv]


========================================
  epiphany
========================================

3.33.90 - August 5, 2019
========================

- New adblocker based on WebKit content extensions (Adrian Perez, #288)
- Close window after drag/drop last tab to another window (#731)
- Adjust address bar popover position (#855)
- Use Alt+Enter for opening pages in new tab (#860)
- Add Ctrl+G shortcut to search bar for next search result (#863)
- Fix search bar text highlighting (#864)
- Redesigned about:overview, the new tab page (Tobias Bernard)
- Add emoji picker context menu item
- Remove context menu item for inserting Unicode control characters
- Fix inappropriate context menu entries for non-downloadable videos
- Fix web app section of the preferences dialog
- On-demand accelerated compositing is once again enabled by default
- Enabled bubblewrap web process sandbox


========================================
  evolution-data-server
========================================

Evolution-Data-Server 3.33.90 2019-08-05
----------------------------------------

Bug Fixes:
	I#112 - Credentials prompter re-asks for credentials of disabled accounts ][ (Milan Crha)
	I#137 - POSIX locale tests fail with ICU 64.x (Milan Crha)
	M!27 - e-book-backend-ldap.c:func_exists() avoid strdup+free (Дилян Палаузов)
	M!28 - Include LDFLAGS in gtkdoc-scangobj command line (Ting-Wei Lan)
	M!29 - WebDAV: Add a way to allow any component with MKCALENDAR (Дилян Палаузов)
	M!30 - ecal: Add camel and libedataserver to the library search path for the gir (Iain Lane)
	M!31 - EWebDAVDiscoverContent: Select implicitly the single item (Дилян Палаузов)

Miscellaneous:
	EWebDAVDiscover can report failure on success and hide collections (Milan Crha)
	e_webdav_discover_dialog_new: Set default response button for the dialog (Milan Crha)
	Let the message-location check also real message location in the search folders (Milan Crha)

Translations:
	Daniel Șerbănescu (ro)
	Rafael Fontenelle (pt_BR)
	Asier Sarasua Garmendia (eu)
	Jordi Mas (ca)


========================================
  gcr
========================================

gcr 3.33.4:
- Move from intltool to gettext [GNOME/gcr#18]
- Fix parameter type for signal handler causing stack smashing on ppc64le [GNOME/gcr!16]
- cleanup: Don't use deprecated g_type_class_add_private() anymore [GNOME/gcr!12]
- Fix GIR annotations [GNOME/gcr!10]
- Fix hashtable ordering assumptions [GNOME/gcr!9]
- build: Fix gcr-trust symbols not appearing in GIR, and hence also VAPI [GNOME/gcr!7]
- Update gcr_pkcs11_get_trust_{store|lookup}_slot URI checks [GNOME/gcr!5]
- build: Update tap scripts for Python 3 compat [GNOME/gcr!2]

========================================
  gjs
========================================

Version 1.57.90
---------------

- New JS API: GLib.Variant has gained a recursiveUnpack() method which
  transforms the variant entirely into a JS object, discarding all type
  information. This can be useful for dealing with a{sv} dictionaries, where
  deepUnpack() will keep the values as GLib.Variant instances in order to
  preserve the type information.

- New JS API: GLib.Variant has gained a deepUnpack() method which is exactly the
  same as the already existing deep_unpack(), but fits with the other camelCase
  APIs that GJS adds.

- Closed bugs and merge requests:
  * Marshalling of GPtrArray broken [#9, !311, Stéphane Seng]
  * Fix locale chooser [!313, Philip Chimento]
  * dbus-wrapper: Remove interface skeleton flush idle on dispose [!312, Marco
    Trevisan]
  * gobject: Use auto-compartment when getting property as well [!316, Florian
    Müllner]
  * modules/signals: Use array destructuring in _emit [!317, Jonas Dreßler]
  * GJS can't call glibtop_init function from libgtop [#259, !319,
    Philip Chimento]
  * GLib's VariantDict is missing lookup [#263, !320, Sonny Piers]
  * toString on an object implementing an interface fails [#252, !299, Marco
    Trevisan]
  * Regression in GstPbutils.Discoverer::discovered callback [#262, !318, Philip
    Chimento]
  * GLib.Variant.deep_unpack not working properly with a{sv} variants [#225,
    !321, Fabián Orccón, Philip Chimento]
  * Various maintenance [!315, Philip Chimento]

- Various CI fixes [Philip Chimento]


========================================
  glib
========================================

Overview of changes in GLib 2.61.2
==================================

* Add various new array functions (#236, #269, #373)
 - `g_array_copy()`
 - `g_ptr_array_copy()`
 - `g_ptr_array_extend()`
 - `g_ptr_array_extend_and_steal()`
 - `g_array_binary_search()`

* Add `g_assert_finalize_object()` helper function for writing tests (#488)

* Rework how D-Bus connections are closed/unreffed when `g_test_dbus_down()` is
  called. Tests which leak a `GDBusConnection` may now time out and abort,
  rather than silently leaking. (#787)

* Add a deprecation macro for GLib macros, and use it; third-party uses of
  long-deprecated GLib macros may now start causing warnings. (#1060)

* Deprecate `GTime` and `GTimeVal`, and various functions which use them.
  Use `GDateTime` and `guint64` UNIX timestamps instead. (#1438)

* Stop using `G_DISABLE_DEPRECATED` to allow disabling deprecation warnings;
  third-party code should now be using
  `GLIB_VERSION_{MIN_REQUIRED, MAX_ALLOWED}` to control symbol usage (!871)

* Improve support for running `ninja test` when GLib is built statically (#1648)

* Improve `GNetworkMonitor` detection of offline states (#1788)

* Fix build failure on macOS related to missing `_g_content_type_get_mime_dirs`
  function (#1791)

* Add various installed utilities’ paths to `gio-2.0.pc` (#1796)

* Fix keyfile `GSettings` backend and portal (especially relevant to any version
  of GLib included in a flatpak runtime) (#1822, !985)

* More IPv6 ‘Happy Eyeballs’ fixes in `GNetworkAddress` (!865)

* Fix CVE-2019-12450, wide permissions of files when copying using GIO (!876)

* Bump the Meson dependency from 0.48.0 to 0.49.2; we won’t depend on anything
  higher than this for a while, as Debian 10 ships 0.49 (!924)

* Various test fixes for Windows (!930, !931)

* Initial support for Universal Windows Platform (UWP): certification, and use
  of packaged libraries (!951)

* Add experimental clang-cl support on Windows, allowing `g_autoptr()` support
  on Windows (!979)

* Bugs fixed:
 - #77 G_STDIO_NO_WRAP_ON_UNIX wraps
 - #236 Add a function to copy an array
 - #269 Additional convenience functions for g_ptr_array
 - #373 GArray could use a binary search function
 - #436 running tests leaves lots of coredumps
 - #453 find-enclosing-mount docs confusing
 - #488 Add g_object_assert_last_unref() helper macro to detect object leaks in tests
 - #590 A reader lock can be obtained even if a writer is already waiting for a lock
 - #638 g_atexit is defined when not declared
 - #737 Initialize GValue in g_object_get_property()
 - #787 gtestdbus: Properly close server connections
 - #804 gdbusproxy prefixes unstripped error
 - #870 Fix and enhance GDatetime for Windows
 - #872 ucs4 functions have wrong return transfer
 - #887 gdusmessage.c mishandles bounds of GDBusMessageType and related enums
 - #894 gvalue: Avoid expensive checks where possible
 - #940 Docs for g_socket_listener_set_backlog are not very helpful
 - #943 G_DEFINE_TYPE_WITH_PRIVATE docs not helpful
 - #1018 Allow guid key in dbus addresses
 - #1060 Add deprecation macro for macros
 - #1169 Tools can display gibberish messages from translations
 - #1270 g_get_charset always returns 8-bit codepage on Windows, crippling UTF-8 output
 - #1438 Deprecate GTimeVal- and GTime-based APIs
 - #1635 g_socket_join_multicast_group iface parameter fails on win32/64
 - #1648 2.58.2: Assorted asserts fail in Arch Linux when built statically
 - #1729 g_content_type_guess segfaults when passed an empty data buffer on Mac OS
 - #1788 GNetworkMonitor claims I am offline
 - #1790 documentation on g_file_info_get_attribute_as_string
 - #1791 _g_content_type_get_mime_dirs missing from libgio-2.0.0.dylib on MacOS
 - #1792 glib-genmarshal generated valist marshal does not respect static scope for some types
 - #1793 glib-genmarshal generates wrong code for va marshaler for VARIANT type
 - #1794 API Proposal: g_timer_is_active
 - #1796 Add gio-querymodules variable to pkg-config file
 - #1797 glib/tests/win32 test failing on 64-bit Visual Studio builds
 - #1798 /contenttype/tree reliably fails on FreeBSD since !863
 - #1807 g_dbus_server_new_sync() documentation references nonexistent function
 - #1808 Stopping a GDBusServer should clean up Unix socket paths (if not abstract) and nonce-tcp nonce files
 - #1811 Introspection info for g_unichar_compose's 3rd arg should be OUT
 - #1822 keyfile gsettings backend not loading
 - #1823 Documentation for disabling selinux is incorrect
 - #1825 GKeyFileSettingsBackend created without filename construct property and unchecked assertion
 - #1828 Small typo in gio manpage
 - #1837 Specify for each (optional) parameter, whether it is OUT or INOUT
 - #1838 Reword documentation for G_DECLARE_FINAL_TYPE
 - #1847 Setting GLIB_VERSION_{MIN_REQUIRED, MAX_ALLOWED} to before 2.56 triggers warnings
 - !533 docs: Document pitfall of deprecation pragmas
 - !563 ci: Add scan-build job in a new ‘analysis’ pipeline stage
 - !678 glib-compile-schemas: Improve translatable strings
 - !817 gdate: Officially mark GTime as deprecated
 - !851 gsettings: Document that lists are returned in no defined order
 - !853 gobject: Fix apostrophe usage in a few small bits of documentation
 - !859 gobject: Add a g_assert_finalize_object() macro
 - !863 gunicollate/cygwin: Don't use __STDC_ISO_10646__ for wchar_t related checks
 - !865 gnetworkaddress: fix "happy eyeballs" logic
 - !867 Post-release version bump
 - !871 Drop G_DISABLE_DEPRECATED
 - !873 Use atomic reference counting for GSource
 - !874 Clamp number of vectors to IOV_MAX / UIO_MAXIOV for GOutputStream writev()...
 - !875 CI/msys2: disable coverage reporting, lcov doesn't support gcc9 yet
 - !876 CVE-2019-12450: gfile: Limit access to files when copying
 - !877 gio: specify proper c_marshaller and va_marshallers
 - !883 tests: Fix small race in GSubprocess tests
 - !884 garcbox.c: Fix typo atomit => atomic
 - !885 goption: Clarify G_OPTION_ARG_FILENAME documentation
 - !889 Include <sys/filio.h> for FIONREAD
 - !901 gmain: Clarify that g_source_destroy() doesn’t drop a reference
 - !904 Add glib-genmarshal tests and fix some valist marshaller bugs
 - !906 property action: Add state hints
 - !908 Improve testfilemonitor test repeatability and debuggability
 - !909 D-Bus auth mechanism improvements
 - !914 ci: Run scan-build in a different build directory
 - !915 docs: fix typo on arrays examples in gvariant-text
 - !917 docs: Fix name of IRC channel in CONTRIBUTING.md
 - !919 glib/tests/refcount.c: Fix tests on non-GCC-isque compilers
 - !920 gmacros: Only use deprecated attributes on enumerators with GCC ≥ 6.5
 - !923 Check if compiler symbols are defined before using them
 - !924 Bump the required version of Meson
 - !930 glib/tests/convert.c: Skip tests that aren't meaningful for Windows
 - !931 glib/tests/fileutils.c: Fix stdio Wrapper Test on Windows
 - !934 build: Increase the slow test timeout to 180s
 - !935 Ignore */__pycache__/* directories
 - !937 Fix module tests on Visual Studio builds
 - !939 gstdio: minor cleanups
 - !941 list model: Expand items-changed docs
 - !944 gutils: Don't limit the length of the host name to 99
 - !945 Avoid overrunning stack at the end of the varargs.
 - !947 gobject/tests/signals.c: Fix tests on Windows
 - !948 GObject: Fix mkenums.py and genmarshal.py tests on Windows
 - !950 ci: Enable CI on FreeBSD 12
 - !951 Preliminary patches for Universal Windows Platform support
 - !952 gio: Make minor docs improvements
 - !953 g_utf8_normalize: Doc comment return missing nullable annotation
 - !954 Fix the ISO 15924 code for Manichaean
 - !955 gmacros: Use _Static_assert when C11 is available
 - !958 gthread: fix minor errno problem in GCond
 - !961 gmain: Fix g_main_context_prepare priority annotation
 - !962 gmacros: Use _Static_assert only for non-expr static assert
 - !964 gmacros.h: Use static_assert on MSVC if possible
 - !968 Fix typo in request handle
 - !970 gdatetime: Unset LC_ALL for the test as well
 - !971 docs.c: Forward link from g_auto* → G_DEFINE_AUTO*
 - !973 doc: fix typo in gio/gresource.c
 - !979 Experimental clang-cl support
 - !980 gmacros.h: Add better support for clang-cl
 - !981 gio: fix typo in g_settings_reset documentation
 - !982 Various doc fixes
 - !985 Keyfile portal fixes
 - !987 gio/tests: Remove code and comments referring to libtool
 - !991 fix atomic detection on older gcc versions
 - !992 docs: Add example to g_test_summary() documentation
 - !994 gio: Fix minor docs mistakes
 - !996 Small array test fixes
 - !997 gdbusaddress: Add missing transfer annotation
 - !1007 Resubmission of !832 “Try to create the complete path right away and fall back”
 - !1009 gapplication: remove inactivity_timeout source on finalize

* Translation updates:
 - Hungarian
 - Indonesian
 - Portuguese (Brazil)
 - Spanish



========================================
  glib-networking
========================================

2.61.90 - August 5, 2019
========================

- Fix translations of certain error messages


========================================
  gnome-bluetooth
========================================

ver 3.33.90:
- Describe "Reveal" as "Open Containing Folder"
- Updated translations


========================================
  gnome-boxes
========================================

3.33.3 - Jul 29, 2019
=====================

Changes since 3.33.2

  - Split new box assistant into two (remote and virtual machines)
  - Use 3.32 Sdk and Runtime for CI
  - Drop option to Share Clipboard (now it is always shared)
  - Add tooltips for media entries in the assistant
  - Added/updated/fixed translations:
    - German
    - Norwegian Bokmål
    - Basque

All contributors to this release:

Asier Sarasua Garmendia <asier.sarasua@gmail.com>
Felipe Borges <felipeborges@gnome.org>
Kjartan Maraas <kmaraas@gnome.org>
Mayank Mandava <mayank.mandava@afterpay.com>
Tim Sabsch <tim@sabsch.com>


========================================
  gnome-desktop
========================================

===============
Version 3.33.90
===============

- Avoid using g_type_class_add_private()
- Updated translations


========================================
  gnome-disk-utility
========================================

3.33.90 - August 5, 2019
========================

nalin.x.linux:
 * Labels for toolbar items added for accessibility

Piotr Drąg:
 * Mark missing string for translation

Updated translations:
 * Daniel Mustieles (es), Daniel Șerbănescu (ro), Jordi Mas (ca),
   Jor Teron (mjw)

3.33.4 - Juli 23, 2019
======================

Iñigo Martínez:
 * build: Various meson updates (many commits)
 * build: Add flatpak manifest
 * ci: Add basic CI support
 * ci: Add support for flatpak building

Kai Lüke:
 * README/INSTALL update and info for Flatpak usage for development

Updated translations:
 * Christian Kirbach (de), Rafael Fontenelle (pt_BR)


========================================
  gnome-getting-started-docs
========================================

===============
Version 3.33.90
===============

* Updates to Getting Started (Petr Kovar, Jakub Steiner)
* Updated translations:
  ca        (Jordi Mas)
  lt        (Petr Kovar)
  ro        (Daniel Șerbănescu)


========================================
  gnome-initial-setup
========================================

3.33.90

* !38 systemd user instance support. This is inert without corresponding
  changes in other GNOME modules, and can be disabled entirely with
  `-Dsystemd=false` at build time.

* Other improvements and bug fixes:
 - !37 summary: don't free borrowed password string
 - !41 data: use a11y menu in initial-setup session
 - !42 driver: Account for multi monitor when checking for small screen
 - !44 language: Refactor the logo selection
 - !45 language: Fix the visibility of the placeholder
 - !46 summary: Make the page fit narrow screens
 - !49 Drop the headers for the first rows
 - !50 account: Drop the avatar button padding
 - !51 Refactor page headers
 - !52 data: Update required gnome-settings-daemon plugins

* Translation updates:
 - Basque
 - Karbi


========================================
  gnome-maps
========================================

3.33.90 - Aug 5, 2019
=========================

Changes since 3.33.90
 - Fix bug going back to already selected place from the search result list
 - Start immediatly at the last viewed location when app was closed last
 - Remember the map type (street or aerial) from when the app was closed last

Added/updated/fixed translations
 - Karbi
 - Romanian
 - Spanish
 - Friulian
 - Basque
 - Brazilian Portuguese
 - Croatian

All contributors to this release
Asier Sarasua Garmendia <asier.sarasua@gmail.com>
Daniel Mustieles <daniel.mustieles@gmail.com>
Daniel Șerbănescu <daniel@serbanescu.dk>
Fabio Tomat <f.t.public@gmail.com>
Goran Vidović <trebelnik2@gmail.com>
James Westman <flyingpimonster@flyingpimonster.net>
Jor Teron <jor.teron@gmail.com>
Marcus Lundblad <ml@update.uu.se>
Rafael Fontenelle <rafaelff@gnome.org>


========================================
  gnome-music
========================================

Overview of changes in 3.33.90
==============================

This release contains a lot of bug fixes following the rewrite of the core of
Music. This work is not yet complete, but the overall state should be
improved. Regressions and known issues are kept track of in #299. Further
reports are welcome.

Some other things were worked on as well:
* Search view rewrite and style update
* Display an empty view if Tracker version is outdated

Thanks to our contributors this release:
 Piotr Drąg
 Jean Felder
 Marinus Schraal

Added translations:
 Karbi

Translations updated:
 Portuguese
 Basque
 Spanish
 Romanian
 Swedish


========================================
  gnome-settings-daemon
========================================

===============
Version 3.33.90
===============
- Translation updates
- Add systemd user service files for all the plugins
- With the exception of gsd-xsettings, daemons do not use the GDK X11
  backend anymore

WWAN:
- New daemon, handles WWAN device settings

Mouse:
- Removed daemon, everything is now implemented in mutter

Clipboard:
- Removed daemon, everything is now implemented in mutter

Color:
- Use Planckian calculations on night light

Rfkill:
- Handle airplane mode for WWAN interfaces

XSettings:
- Round Xft.dpi setting to an integer
- Add entry for overlay scrolling setting

Media-keys:
- Use device node from AcceleratorActivated arguments


========================================
  gnome-user-docs
========================================

3.33.90
=======
* Updates to GNOME Help (Michael Hill, David King, Petr Kovar, Jim Campbell,
  Sebastian Rasmussen)
* Updates to System Admin Guide (Petr Kovar)
* Updated translations:
  ca        (Jordi Mas)
  es        (Daniel Mustieles)
  pl        (Piotr Drąg)
  pt_BR     (David King)
  sv        (Anders Jonsson)


========================================
  gnome-video-effects
========================================


version 0.5.0
  - Port to meson build system (thanks Jeremy!)
  - Various updated translations

========================================
  gnome-weather
========================================

3.33.90
=======
* Export location information to GNOME Shell (Florian Müllner)
* Use weather station's time zone (Jim Mason)
* Allow enter key to show location (James Westman)


========================================
  gsettings-desktop-schemas
========================================

Major changes in 3.33.90
========================
- Add 'middle-click-emulation' setting
- Translation updates


========================================
  gtk-doc
========================================

GTK-Doc 1.31  (Aug 5 2019)
===============

Nonmaintainer release to fix "Wrong permissions for style CSS file" (#84)


========================================
  gvfs
========================================

Major changes in 1.41.90
========================
* udisks2: Change display name for crypto_unknown devices
* google: Disable deletion of non-empty directories
* google: Fix crashes when deleting if the file isn't found
* google: Fix issue with stale entries remaining after rename operation
* build: Define gvfs_rpath for libgvfsdaemon.so
* proxy: Don't leak a GVfsDBusDaemon
* Translation updates


========================================
  libdazzle
========================================

==============
Version 3.33.4
==============

Changes in this release:

 • Work around recent changes in GTK action muxing
 • Autoptr additions
 • Improve fuzzy text matches
 • Various fixes for alternative compilers


========================================
  libgee
========================================

libgee 0.20.2
=============
 * Minor build fixes


========================================
  libgudev
========================================


CHANGES WITH 233:
        * Require glib 2.38
        * Small documentation updates
        * Remove gnome-common build dependency

========================================
  librsvg
========================================

Version 2.45.90
- New API functions:
    rsvg_handle_render_document()
    rsvg_handle_render_layer()
    rsvg_handle_render_element()
    rsvg_handle_get_geometry_for_layer()
    rsvg_handle_get_geometry_for_element()

  CairoRenderer in the librsvg_crate has corresponding functions
  as well.

- Fix builds with gettext ≥ 0.20 (Ting-Wei Lan).

- If the C API is called out of order, downgrade hard panics to
  g_critical() to cope with incorrect/old applications that called
  rsvg_handle_get_dimensions() before rsvg_handle_close().

- API reference documentation is much improved.


========================================
  libsoup
========================================

Changes in libsoup from 2.67.90 to 2.67.91:

        * HSTS: New API to retrieve information about existing HSTS policies
          [Claudio Saavedra]

        * Updated translations: French, Romanian, Spanish

Changes in libsoup from 2.67.3 to 2.67.90a:

        * WebSockets: add support for WebSocket extensions via new
          SoupWebsocketExtensionManager and SoupWebsocketExtension API
          [Carlos Garcia Campos]

        * WebSockets: add support for the permessage-deflate extension, enabled
          by default in SoupServer and in the client side only if SoupWebsocketExtensionManager
          is added to a session [Carlos Garcia Campos]

        * WebSockets: Allow sending close frames with no body [Carlos Garcia Campos]

        * WebSockets: ignore messages after close has been sent and received [Carlos Garcia Campos]

        * Meson: tls-check improved [Xavier Claessens]

        * Meson: improve Apache checks [Claudio Saavedra]


========================================
  orca
========================================

3.33.90 - 2 August 2019

Web:

 * Try to include results count during find in page searches

 * Fix bugs preventing SayAll on page load from working

 * Fix bug in braille presentation of new Firefox location input

 * Work around another instance of CSSed text being exposed as one char
   per line

 * Improve presentation of treegrids

 * Reduce chattiness with editable comboboxes and with auto-focused
   descendants

 * Never treat layout-only-table cells as focus-mode widgets

 * Make finding clickables more performant

 * Ensure we present final word in element when navigating by word

 * Present caret-moved events from mouse clicks even if element hasn't
   changed

 * Ongoing work on Chromium script. Please note: ATK support in Chromium
   needs much work. Until that work has been done, Orca will not be able
   to provide access to Chromium. The current support is very much a work
   in progress and not yet ready for end-user testing.

Mouse Review:

 * Improve overall presentation of units of text under the pointer

 * Improve logic filtering out irrelevant mouse movements during review

 * Apply non-mousereview cell/row presentation preferences for a
   more consistent experience

 * Don't present description for mouse review if tooltip presentation
   is off

General:

 * Add support for Shift Lock

 * Add initial support for AtspiText's scrollSubstringTo

 * Add fall backs for click-at-point failures

 * Also check for "underline:spelling" as indication of spelling error

 * Improve response time for mouse input events

 * Allow building with gettext ≥ 0.20


New and updated translations (THANKS EVERYONE!!!):

    es            Spanish               Daniel Mustieles
    hu            Hungarian             Attila Hammer
    id            Indonesian            Kukuh Syafaat
    pt_BR         Brazilian Portuguese  Rafael Fontenelle
    ro            Romanian              Florentina Mușat
    ru            Russian               Stas Solovey
    sl            Slovenian             Matej Urbančič
    sv            Swedish               Anders Jonsson

=========


========================================
  pango
========================================

Overview of changes in 1.44.3
=============================
- Install pango-ot headers
- Make subpixel positioning optional
- fc: Ignore fonts with unsupported formats

Overview of changes in 1.44.2
=============================
- Disable ligatures when letterspacing
- Set design coords on hb_font_t
- Expose more font options in pango-view
- OS X: Make 'system-ui' font work
- Keep deprecated pango-fc apis in headers
- Make hex boxes work, always
- introspection: Various build fixes
- introspection: Add PangoPT, PangoFT2 namespaces
- layout: Make the new line-spacing opt-in

Overview of changes in 1.44.1
=============================
- Fix a crash with allow_break attributes
- Fix Emoji spacing
- Fix up includes and pkg-config requires
- Correct some cases for hyphen insertion

Overview of changes in 1.44.0
=============================
- Use harfbuzz for shaping on all platforms
- Stop using freetype for font loading; this
    drops support for type1 and bitmap fonts
- Add a getter for hb_font_t
- Make PangoCoverage a GObject
- Add a pango_tailor_break api
- font metrics: Add line height
- layout: Support line spacing
- layout: Draw hyphens for line breaks
- Add an attribute to suppress line breaking
- cairo: Don't render hex boxes for space
- Add an attribute to show invisible characters
- Stop quantizing glyph positions
- Add tests for itemization and line breaking
- Remove language and shape engine remnants
- Rename meson options: gtk_doc, introspection
- Require GLib 2.59.2
- Require Harfbuzz 2.0


========================================
  pyatspi
========================================

What's new in pyatspi 2.33.90:

* Fix typo in get_imageLocale.

* event.str(): show event sender.

* Remove wrapper for atspi_text_notify_reading_position, as the function has
  been removed from at-spi2-core.

* events: replace object:text:reading-position with
  screen-reader:region-changed.

* Add wrapper for atspi_set_reference_window.


========================================
  simple-scan
========================================

Overview of changes in simple-scan 3.33.90

  * Change (user visible) name to "Document Scanner"
  * Updated icon


========================================
  totem
========================================


Major changes in 3.33.90:
- Update application icon
- Use title case for the "Open Containing Folder" menu item
- Disable VAAPI support for now, it's too buggy

========================================
  vala
========================================

Vala 0.45.90
============
 * Various improvements and bug fixes:
  - vala:
    + Support static methods in error-domains [#829]
    + Fix mixup of target_glib_major/minor in set_target_glib_version() [#825]
    + Implicit GValue cast requires GOBJECT profile
    + NoAccessorMethod checks require GOBJECT profile
    + 'construct' is not supported in POSIX profile
  - codegen:
    + Use G_TYPE_CHECK_INSTANCE_CAST for comparisons with interfaces
    + Append line-break after G_DEFINE_AUTOPTR_CLEANUP_FUNC
    + Move private type-struct to type-definition section
    + Include required type-definition when casting from generic pointer [#828]
  - girparser: Handle "function-macro" by skipping them [gi#159]
  - valadoc: Install icons and doclets to API dependent folders

 * Bindings:
  - glib-2.0: Add new symbols and deprecations from 2.62
  - glib-2.0: Add MappedFile.from_fd constructor [#824]
  - gstreamer: Update from 1.17.0+ git master
  - posix: Fix return-value of mknod() and c-include for tcgetsid()
  - posix: Add *at() calls and related constants [#823]
  - webkit2gtk-4.0: Fix WebContext.initialize_notification_permissions()
  - x11: Fix return type of XInternAtoms and XGetAtomNames bindings
  - vapi: Update GIR-based bindings


